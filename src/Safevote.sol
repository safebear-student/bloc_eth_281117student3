pragma solidity ^0.4.11;

contract Safevote {
    mapping (bytes32 => uint8) public votesReceived;
    bytes32[] public candidateList;

    function Safevote(bytes32[] candidateNames) {
        candidateList = candidateNames;
       
    }

    function voteForCandidate(bytes32 candidate) returns (uint8) {
        require (validateCandate(candidate) == true);
        votesReceived[candidate] += 1;
    }

    function totalVotesFor(bytes32 candidate) returns (uint8) {
        require (validateCandate (candidate) == true);
        return votesReceived[candidate];

    }

    function validateCandate(bytes32 candidate) returns (bool) {
        for(uint i = 0; i < candidateList.length; i++) {
            if (candidateList[i] == candidate) {
                return true;
            }
        }
        return false;
    }



}
